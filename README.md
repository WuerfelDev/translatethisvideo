# TranslateThisVideo

Telegram bot to translate spoken text of videos to English  using vosk and deepl

## Installation
Since the bot has to be able to download large files we need to use the local bot API server (authentificates as new custom client, then provides a not-limited API locally). To install the bot follow these steps:

1. Install the local API server: https://github.com/tdlib/telegram-bot-api
2. Run the bot for the first time: `python ./telegram-bot.py`. It will ask for all the keys listed below and proceed to download the language models.

### API Keys and Tokens

1. `api_id` and `api_hash`: Follow the instructions on https://core.telegram.org/api/obtaining_api_id#obtaining-api-id
2. Telegram bot token: Create a bot using [@BotFather](https://t.me/BotFather)
3. Deepl API Free: https://www.deepl.com/pro-api

## Usage
```
python ./telegram-bot.py
```

Without much effort you can change the languages that are offered for translation. Edit the `languages` dict and use a valid [hosted vosk model](https://alphacephei.com/vosk/models). On the next start it will be download and installed for you. 


## Motivation

While following the war in Ukraine closely I noticed that it is not easy to understand the context of shared videos. The goal for this bot is to translate the spoken text. For many reasons it might not always be super acurate but it still provides a better understandig of the situation.
Slava Ukraini! 🇺🇦

## Resources

- https://github.com/eternnoir/pyTelegramBotAPI
- https://core.telegram.org/bots/api
- https://github.com/alphacep/vosk-api/tree/master/python
- https://www.deepl.com/de/pro-api
