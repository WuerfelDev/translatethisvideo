from vosk import Model, KaldiRecognizer, SetLogLevel
import atexit
import configparser
import deepl
import json
import logging
import os
import psutil
import subprocess
import telebot
import time

# Logging + Debugging
debugmode = False
if debugmode:
    logger = telebot.logger
    telebot.logger.setLevel(logging.DEBUG)
    SetLogLevel(1)

# Kill telegram-bot-api if we started it
def cleanup(localbotapi):
    localbotapi.kill()
    print('cleaned up!')


# Load CONFIG_APIS
config = configparser.ConfigParser()
config.read('CONFIG_APIS')
write_config = False

localbotport = 8081
languages = {"Ukrainian":'vosk-model-uk-v3',"Russian":'vosk-model-small-ru-0.22'} # Keep this up-to-date: https://alphacephei.com/vosk/models#model-list


if subprocess.run(["which", "telegram-bot-api"],stdout=subprocess.DEVNULL).returncode != 0:
    print("Can't find telegram-bot-api. Please install it.")
    exit(1)

# Check for keys
if not 'tg_api_id' in config['DEFAULT']:
    print("Please enter your Telegram API ID:")
    config['DEFAULT']['tg_api_id'] = input()
    write_config = True

if not 'tg_api_hash' in config['DEFAULT']:
    print("Please enter your Telegram API hash:")
    config['DEFAULT']['tg_api_hash'] = input()
    write_config = True

if not 'tg_bot_token' in config['DEFAULT']:
    print("Please enter your Telegram bot token:")
    config['DEFAULT']['tg_bot_token'] = input()
    write_config = True

if not 'deepl' in config['DEFAULT']:
    print("Please enter your Deepl API key:")
    config['DEFAULT']['deepl'] = input()
    write_config = True

# Write api keys to config file
if write_config:
    with open('CONFIG_APIS', 'w') as configfile:
        config.write(configfile)

# Set up vosk language models
for lang,code in languages.items():
    if not os.path.isdir('models/'+code):
        print("Downloading model for "+lang)
        os.system('wget -P models https://alphacephei.com/kaldi/models/'+code+'.zip')
        os.system('unzip models/'+code+'.zip -d models/')
        os.system('rm models/'+code+'.zip')
        print("done.")

if "telegram-bot-api" in (i.name() for i in psutil.process_iter()):
    print("telegram-bot-api already running.")
else:
    print("Starting telegram-bot-api")
    if not os.path.isdir('.tg-api-files'):
        os.mkdir('.tg-api-files')
    localbotapi = subprocess.Popen(['telegram-bot-api','--local','--http-port',str(localbotport),'--dir','.tg-api-files','--api-id',config['DEFAULT']['tg_api_id'],'--api-hash',config['DEFAULT']['tg_api_hash']])
    atexit.register(cleanup, localbotapi)
    time.sleep(1)


# Set up language models
sample_rate=16000
recognizer = {}
for lang,code in languages.items():
    print("Loading model for "+lang)
    model = Model("models/"+code)
    kaldi_rec = KaldiRecognizer(model, sample_rate)
    recognizer[lang] = kaldi_rec;
print('done.')

# Initialize translator
translator = deepl.Translator(config['DEFAULT']['deepl'])

telebot.apihelper.API_URL = "http://localhost:"+str(localbotport)+"/bot{0}/{1}"
bot = telebot.TeleBot(config['DEFAULT']['tg_bot_token'])


@bot.message_handler(commands=['start','help'])
def send_welcome(message):
    bot.send_message(message.chat.id, "Hello!\nJust send or forward me a video in _"+"_ or _".join(languages.keys())+"_ and I will try to translate it for you", parse_mode='Markdown')


@bot.message_handler(commands=['about','info'])
def send_about(message):
    bot.send_message(message.chat.id, "*Translate This Video Bot*\nThis bot uses Vosk language models to understand language spoken in videos. In this bot the models for _"+"_ and _".join(languages.keys())+"_ are available. The text is then passed to DeepL for translation. The code open source and you can read it here: https://gitlab.com/WuerfelDev/TranslateThisVideo", parse_mode='Markdown',disable_web_page_preview=True)


@bot.message_handler(func=lambda m:debugmode,commands=['usage','quota'])
def send_usage(message):
    usage = translator.get_usage()
    bot.send_message(message.chat.id, "Current usage stats: {} of {} characters".format(usage.character.count,usage.character.limit))

@bot.message_handler(func=lambda m:debugmode,commands=['logout'])
def log_out_of_bot(message):
    bot.reply_to(message, "Logging out!")
    bot.log_out()
    exit(1)

def test_mimetype(message):
    return message.document.mime_type == 'video/mp4'

@bot.message_handler(content_types=['video','video_note','audio','voice'])
@bot.message_handler(func=test_mimetype, content_types=['document'])
def handle_sent_video(message):
    markup = telebot.types.InlineKeyboardMarkup(row_width=2) # row_width not working, workaround below
    lastlang = None
    for lang in languages:
        if lastlang == None:
            lastlang = lang
        else:
            markup.row(telebot.types.InlineKeyboardButton(text=lastlang, callback_data=lastlang),telebot.types.InlineKeyboardButton(text=lang, callback_data=lang))
            lastlang = None
    if lastlang != None:
        markup.row(telebot.types.InlineKeyboardButton(text=lastlang, callback_data=lastlang))
    bot.reply_to(message, "Which language is this video in?", reply_markup=markup)


@bot.callback_query_handler(func=lambda call: True)
def test_callback(call):
    print('Processing!')
    msg = bot.reply_to(call.message.reply_to_message, "*Translation from " + call.data + ":*\n\n_Processing..._", parse_mode='Markdown')
    # Download file
    if call.message.reply_to_message.video != None:
        file_id = call.message.reply_to_message.video.file_id
    elif call.message.reply_to_message.document != None:
        file_id = call.message.reply_to_message.document.file_id
    elif call.message.reply_to_message.video_note != None:
        file_id = call.message.reply_to_message.video_note.file_id
    elif call.message.reply_to_message.voice != None:
        file_id = call.message.reply_to_message.voice.file_id # Bonus :)
    elif call.message.reply_to_message.audio != None:
        file_id = call.message.reply_to_message.audio.file_id
    file_info = bot.get_file(file_id)

    source_list = run_vosk(call.data, file_info.file_path)
    if not translator.get_usage().character.limit_exceeded:
        translation_list = translator.translate_text(source_list, target_lang='EN-US')
        translation_string = '\n'.join([t.text for t in translation_list])
        message = "*Translation from " + call.data + ":*\n\n"+translation_string
        bot.answer_callback_query(call.id, "Translation complete!")
    else:
        source_string = '\n'.join(source_list)
        message = "Translation api quota exceeded. Your current result in "+call.data+" is:\n\n"+source_string

    bot.edit_message_text(chat_id=msg.chat.id, message_id=msg.message_id, text=message, parse_mode='Markdown',disable_web_page_preview=True)
    print('done.')


def run_vosk(lang, filepath):
    result = []
    rec = recognizer[lang]
    process = subprocess.Popen(['ffmpeg', '-loglevel', 'quiet', '-i', filepath,
                            '-ar', str(sample_rate) , '-ac', '1', '-f', 's16le', '-'],
                            stdout=subprocess.PIPE)
    while True:
        data = process.stdout.read(4000)
        if len(data) == 0:
            break
        if rec.AcceptWaveform(data):
            text = json.loads(rec.Result())['text']
            if text != "":
                result.append(text)

    result.append(json.loads(rec.FinalResult())['text'])
    return result


bot.infinity_polling()
